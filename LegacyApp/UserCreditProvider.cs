﻿using System;

namespace LegacyApp
{
    public class UserCreditProvider : IUserCreditProvider
    {
        public int GetCreditLimit(string firstname, string surname, DateTime dateOfBirth)
        {
            using var userCreditService = new UserCreditServiceClient();
            return userCreditService.GetCreditLimit(firstname, surname, dateOfBirth);
        }
    }
}