﻿using System;

namespace LegacyApp
{
    public class User : ICloneable
    {
        public Client Client { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string EmailAddress { get; set; }
        public string Firstname { get; set; }
        public string Surname { get; set; }
        public bool HasCreditLimit { get; set; }
        public int CreditLimit { get; set; }

        public object Clone() => MemberwiseClone();
    }
}