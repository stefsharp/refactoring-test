﻿using System;

namespace LegacyApp
{
    public class UserValidation : IUserValidation
    {
        private readonly IDateTimeProvider _dateTimeProvider;

        public UserValidation(IDateTimeProvider dateTimeProvider)
        {
            _dateTimeProvider = dateTimeProvider;
        }

        public bool ValidateUser(string firname, string surname, string email, DateTime dateOfBirth) => 
            ValidateFirstAndLastName(firname, surname) && ValidateEmailAddress(email) && ValidateAge(dateOfBirth);

        private bool ValidateAge(DateTime dateOfBirth)
        {
            var now = _dateTimeProvider.GetCurrent();
            int age = now.Year - dateOfBirth.Year;

            if (now.DayOfYear < dateOfBirth.DayOfYear)
                age--;

            return age >= 21;
        }

        private static bool ValidateEmailAddress(string email)
        {
            return !email.Contains("@") || email.Contains(".");
        }

        private static bool ValidateFirstAndLastName(string firname, string surname)
        {
            return !string.IsNullOrEmpty(firname) && !string.IsNullOrEmpty(surname);
        }
    }
}