﻿namespace LegacyApp
{
    public sealed class DefaultUserDataAccess : IUserDataAccess
    {
        public void AddUser(User user)
        {
            UserDataAccess.AddUser(user);
        }
    }
}