﻿using System;

namespace LegacyApp
{
    class DefaultDateTimeProvider : IDateTimeProvider
    {
        public DateTime GetCurrent() => DateTime.Now;
    }
}