﻿using System;

namespace LegacyApp
{
    public interface IUserValidation
    {
        bool ValidateUser(string firname, string surname, string email, DateTime dateOfBirth);
    }
}