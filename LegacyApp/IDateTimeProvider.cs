﻿using System;

namespace LegacyApp
{
    public interface IDateTimeProvider
    {
        DateTime GetCurrent();
    }
}