﻿using System;

namespace LegacyApp
{
    public class UserService
    {
        private readonly UserServiceV2 _newService;

        public UserService()
            : this(new UserValidation(new DefaultDateTimeProvider()), new ClientRepository(), new UserCreditProvider(), new DefaultUserDataAccess(), 500)
        {
        }

        public UserService(IUserValidation userValidation, IClientRepository clientRepository, IUserCreditProvider userCreditProvider, IUserDataAccess userDataAccess, int minimumCreditLimit)
        {
            _newService = new UserServiceV2(userValidation, clientRepository, userCreditProvider, userDataAccess, minimumCreditLimit);
        }

        public bool AddUser(string firstname, string surname, string email, DateTime dateOfBirth, int clientId) => 
            _newService.AddUser(firstname, surname, email, dateOfBirth, clientId);
    }
}