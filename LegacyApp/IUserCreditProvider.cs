﻿using System;

namespace LegacyApp
{
    public interface IUserCreditProvider
    {
        int GetCreditLimit(string firstname, string surname, DateTime dateOfBirth);
    }
}