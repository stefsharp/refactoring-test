﻿using System;

namespace LegacyApp
{
    /*
     * The main goal of this refactoring is to make the UserService legacy class
     * testable. We use dependency injection and unit test every injected service
     * separately. We apply the single responsibility pattern to every service. We
     * keep interfaces to the minimum method count: 1. We try to make classes closed
     * for modification and open for extension. We keep the different layers as simple
     * as possible by providing only the absolutely required members. Before refactoring
     * a part of the original class, we write unit tests that will ensure that our
     * refactoring will not break any existing rules. finally, we inject the original
     * behavior to the new class so it behaves exactly like the previous one. Only, this time,
     * it is much more easier for a developer to extend the UserServiceV2 with new validation
     * rules, new data access, new repository access, and so on.
     */
    public class UserServiceV2
    {
        private readonly IUserValidation _userValidation;
        private readonly IClientRepository _clientRepository;
        private readonly IUserCreditProvider _userCreditProvider;
        private readonly IUserDataAccess _userDataAccess;
        private readonly int _minimumCreditLimit;

        public UserServiceV2(IUserValidation userValidation, IClientRepository clientRepository, IUserCreditProvider userCreditProvider, IUserDataAccess userDataAccess, int minimumCreditLimit)
        {
            _minimumCreditLimit = minimumCreditLimit;
            _userDataAccess = userDataAccess;
            _userCreditProvider = userCreditProvider;
            _clientRepository = clientRepository;
            _userValidation = userValidation;
        }

        public bool AddUser(string firstname, string surname, string email, DateTime dateOfBirth, int clientId)
        {
            if (!_userValidation.ValidateUser(firstname, surname, email, dateOfBirth))
                return false;

            var fullUser = CreateUser(firstname, surname, email, dateOfBirth, clientId);

            if (fullUser.HasCreditLimit && fullUser.CreditLimit < _minimumCreditLimit)
                return false;
            
            _userDataAccess.AddUser(fullUser);

            return true;
        }

        private User CreateUser(string firstname, string surname, string email, DateTime dateOfBirth, int clientId)
        {
            Client client = _clientRepository.GetById(clientId);

            var user = new User
            {
                Client = client,
                DateOfBirth = dateOfBirth,
                EmailAddress = email,
                Firstname = firstname,
                Surname = surname
            };

            User fullUser = ApplyClientCreditLimit(GetClientType(client), user);

            return fullUser;
        }

        private ClientTypes GetClientType(Client client)
        {
            if (Enum.TryParse(client.Name, out ClientTypes clientType))
                return clientType;

            return ClientTypes.RegularClient;
        }

        private User ApplyClientCreditLimit(ClientTypes clientType, User user)
        {
            var fullUser = (User)user.Clone();
            fullUser.HasCreditLimit = true;

            if (clientType == ClientTypes.VeryImportantClient)
            {
                // Skip credit check
                fullUser.HasCreditLimit = false;
                return fullUser;
            }

            //Set the credit limit for the user.
            int creditLimit = _userCreditProvider.GetCreditLimit(user.Firstname, user.Surname, user.DateOfBirth);
            fullUser.CreditLimit = creditLimit;

            // Do credit check and double credit limit for important clients.
            if (clientType == ClientTypes.ImportantClient)
                fullUser.CreditLimit *= 2;

            return fullUser;
        }
    }
}
