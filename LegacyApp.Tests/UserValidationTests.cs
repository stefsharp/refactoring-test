﻿using FluentAssertions;
using NSubstitute;
using System;
using Xunit;

namespace LegacyApp.Tests
{
    public sealed class UserValidationTests
    {
        private readonly IDateTimeProvider _dateTimeProvider = Substitute.For<IDateTimeProvider>();
        private readonly UserValidation _userValidation;

        public UserValidationTests()
        {
            _dateTimeProvider.GetCurrent().Returns(DateTime.Now);
            _userValidation = new UserValidation(_dateTimeProvider);
        }

        [Theory]
        [InlineData(null, "John", "test@test.com")]
        [InlineData("", "John", "test@test.com")]
        [InlineData("username", null, "test@test.com")]
        [InlineData("username", "", "test@test.com")]
        [InlineData("username", "John", "test@test")]
        public void EnsureValidationIsPerformedOnUsernameSurnameAndEmail(string username, string surname, string email)
        {
            DateTime birthDate = DateTime.Now;
            bool res = _userValidation.ValidateUser(username, surname, email, birthDate);

            res.Should().BeFalse();
        }

        [Fact]
        public void EnsureUserIsAtLeast20YearsOld()
        {
            for (int i = 0; i > -21; i--)
            {
                DateTime birthDate = DateTime.Now.AddYears(i);
                bool res = _userValidation.ValidateUser("username", "surname", "test@test.com", birthDate);

                res.Should().BeFalse();
            }
        }

        [Fact]
        public void EnsureUserIsNot21Tomorrow()
        {
            DateTime birthDate = DateTime.Now.AddYears(-21).AddDays(1);
            bool res = _userValidation.ValidateUser("username", "surname", "test@test.com", birthDate);

            res.Should().BeFalse();
        }

        [Fact]
        public void EnsureUserIsValidIfAtLeast21YearsOld()
        {
            DateTime birthDate = DateTime.Now.AddYears(-21).AddDays(-1);
            bool res = _userValidation.ValidateUser("username", "surname", "test@test.com", birthDate);

            res.Should().BeTrue();
        }

        [Fact]
        public void EnsureUserIsNot21NextMonth()
        {
            DateTime birthDate = DateTime.Now.AddYears(-21).AddMonths(1);
            bool res = _userValidation.ValidateUser("username", "surname", "test@test.com", birthDate);

            res.Should().BeFalse();
        }
    }
}
