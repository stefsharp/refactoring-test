using System;
using Bogus;
using FluentAssertions;
using NSubstitute;
using Xunit;

namespace LegacyApp.Tests
{
    public class UserServiceV2Tests
    {
        private const int MinimumCreditLimit = 500;

        private readonly UserServiceV2 _userService;
        private readonly IUserValidation _userValidation = Substitute.For<IUserValidation>();
        private readonly IClientRepository _clientRepository = Substitute.For<IClientRepository>();
        private readonly IUserCreditProvider _userCreditProvider = Substitute.For<IUserCreditProvider>();
        private readonly IUserDataAccess _userDataAccess = Substitute.For<IUserDataAccess>();

        public UserServiceV2Tests()
        {
            _userService = new UserServiceV2(_userValidation, _clientRepository, _userCreditProvider, _userDataAccess, MinimumCreditLimit);
        }

        [Fact]
        public void EnsureValidationIsPerformed()
        {
            //Arrange
            _userValidation.ValidateUser(default, default, default, default).ReturnsForAnyArgs(false);

            //Act
            bool res = _userService.AddUser(null, null, null, DateTime.Now, -1);
            
            //Assert
            res.Should().BeFalse();
        }

        [Theory]
        [InlineData("VeryImportantClient", 1000, 1000000, true, false, 0)]
        [InlineData("ImportantClient", 1000, 1000000, true, true, 2)]
        [InlineData("SomeClient", 1000, 1000000, true, true, 1)]
        [InlineData("SomeBadClient", 50, 200, false, false, 0)]
        public void EnsureImportantClientCreditLimitIsCalculatedProperly(string clientName, int minCreditLimit, int maxCreditLimit, bool expectedResult, bool expectedHasCreditLimit, int expectedCreditLimitFactor)
        {
            var faker = new Faker();
            var user = new Faker<User>()
                .RuleFor(u => u.Firstname, f => f.Name.FirstName())
                .RuleFor(u => u.Surname, f => f.Name.LastName())
                .RuleFor(u => u.DateOfBirth, f => f.Date.Past(25))
                .RuleFor(u => u.EmailAddress, f => f.Internet.Email())
                .Generate();
            var client = new Faker<Client>()
                .RuleFor(c => c.Id, f => f.Random.Int(1))
                .RuleFor(c => c.ClientStatus, f => f.PickRandom<ClientStatus>())
                .RuleFor(c => c.Name, clientName)
                .Generate();
            _clientRepository.GetById(client.Id).Returns(client);
            _userValidation.ValidateUser(default, default, default, default)
                .ReturnsForAnyArgs(true);
            int creditLimit = faker.Random.Int(minCreditLimit, maxCreditLimit);
            _userCreditProvider.GetCreditLimit(user.Firstname, user.Surname, user.DateOfBirth).Returns(creditLimit);

            bool res = _userService.AddUser(user.Firstname, user.Surname, user.EmailAddress, user.DateOfBirth, client.Id);

            res.Should().Be(expectedResult);
            if (!res)
                _userDataAccess.DidNotReceiveWithAnyArgs().AddUser(default);
            else
            {
                _userDataAccess.Received(1).AddUser(Arg.Is<User>(u =>
                    u.HasCreditLimit == expectedHasCreditLimit &&
                    u.CreditLimit == expectedCreditLimitFactor * creditLimit &&
                    u.Firstname == user.Firstname &&
                    u.Surname == user.Surname &&
                    u.EmailAddress == user.EmailAddress &&
                    u.DateOfBirth == user.DateOfBirth));
            }
        }
    }
}
